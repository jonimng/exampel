import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';

import 'rxjs/Rx';

@Injectable()
export class UsersService {

  http:Http;

  getUsers(){
    //return ['Message1','Message2','Message3','Message4'];
    //get messages from the slim rest api (Don't say DB
    return this.http.get("http://localhost:8020/slim/users");
    
    }
  getUser(id){
      let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded',
        
      })
    }
    
  //return ['Message1','Message2','Message3','Message4'];
  //get messages from the slim rest api (Don't say DB)
  return this.http.get("http://localhost:8020/slim/users/"+id,options);
  } 
  putUser(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
        
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.put("http://localhost:8020/slim/users/"+ key,params.toString(), options);
  }
  postUser(data){
      let options = {
        headers:new Headers({
          'content-type':'application/x-www-form-urlencoded'
        })
      }
      let params = new HttpParams().append('username',data.username).append('phone',data.phone_number);
      console.log(params);
      return this.http.post("http://localhost:8020/slim/users",
        params.toString(),
        options);
  
    }
  deleteUser(key){
      
          return this.http.delete("http://localhost:8020/slim/users/"+key);
        }

  constructor(http:Http) {
    
    this.http = http;
   }

}
