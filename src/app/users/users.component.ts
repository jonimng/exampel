import { UsersService } from './users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users;
  usersKeys;
  constructor(private service:UsersService) {
    service.getUsers().subscribe(
      response=>{
       //console.log(response.json())
       this.users = response.json();
       this.usersKeys  = Object.keys(this.users);

       
       }
    );
  }
  deleteUser(key){
    
        console.log(key);
        let index = this.usersKeys.indexOf(key);
        this.usersKeys.splice(index,1);    
        this.service.deleteUser(key).subscribe(      
          response=>{
            console.log(response)
            }); 
    
    
      }

  ngOnInit() {
  }

}
