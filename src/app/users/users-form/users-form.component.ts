import { UsersService } from './../users.service';
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'usersForm',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {
  
    @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); 
  
    @Output() addUserps:EventEmitter<any> = new EventEmitter<any>();
    
    service:UsersService;
  
    usrform = new FormGroup({
      username:new FormControl(),
      phone_number:new FormControl(),
    });
  
    sendData(){
      this.addUser.emit(this.usrform.value.username);
      console.log(this.usrform.value);
      this.service.postUser(this.usrform.value).subscribe(
        response => {
          console.log(response.json())
          this.addUserps.emit();
        }
      )
    };
  
    constructor(service:UsersService, private formBuilder:FormBuilder) {
      this.service=service;
     }
  
    ngOnInit() {
      		this.usrform = this.formBuilder.group({
           username:  [null, [Validators.required]],
              phone_number : [null, Validators.required],
    });
  }
  
}
