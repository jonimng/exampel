import { UsersService } from './../users.service';
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormGroup, FormControl,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'userForm',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  
    @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); 
  
    @Output() addUserps:EventEmitter<any> = new EventEmitter<any>();
    
    service:UsersService;
  
    usrform = new FormGroup({
      username:new FormControl(),
      phone_number:new FormControl(),
    });
  
    sendData(){
      this.addUser.emit(this.usrform.value.message);
      console.log(this.usrform.value);
      this.route.paramMap.subscribe(params=>{
        let id = params.get('id');
        this.service.putUser(this.usrform.value, id).subscribe(
          response => {
            console.log(response.json());
            this.addUserps.emit();
          }
        );
      })
    }
  
    constructor( private route: ActivatedRoute, service:UsersService, private formBuilder:FormBuilder) {
      this.service=service;
     }
    user;
    ngOnInit() {
      		this.usrform = this.formBuilder.group({
           username:  [null, [Validators.required]],
              phone_number : [null, Validators.required],
    });
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
})
  }
  
}