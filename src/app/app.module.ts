import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {NgModel, FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UsersComponent } from './users/users.component';
import { NevigationComponent } from './nevigation/nevigation.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { HttpModule } from '@angular/http';
import { UserComponent } from './users/user/user.component';
import { UserFormComponent } from './users/user-form/user-form.component';


@NgModule({
  declarations: [ 
    AppComponent,
    ProductsComponent,
    NotFoundComponent,
    UsersComponent,
    NevigationComponent,
    UsersFormComponent,
    UserComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'',component:ProductsComponent},
      {path:'products',component:ProductsComponent},
      {path:'users',component:UsersComponent},
      {path:'user/:id',component:UserComponent},
      {pathMatch:'full',path:'user-form/:id',component:UserFormComponent},
      {path:'**',component:NotFoundComponent}
    ])
  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
