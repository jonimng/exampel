import { Component } from '@angular/core';
import { NevigationComponent } from './nevigation/nevigation.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
